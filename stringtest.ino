// declare two separate but identical strings
// compiler will usually notice this and point them to the same storage
static char * x = "test";
static char * y = "test";

// declare another separate string but not with the same contents
// the compiler will have to store this at a different address
static char z[5];

// compare two strings, showing the contents, address, and whether those match
static void
compare(
  char *  s1, // pointer to first string
  char *  s2) // pointer to second string
{
  // show contents of the two strings
  Serial.print("contents of first  string:  \"");
  Serial.print(s1);
  Serial.println("\"");
  Serial.print("contents of second string:  \"");
  Serial.print(s2);
  Serial.println("\"");
  
  // show the addresses of the two strings (where they're stored in memory)
  Serial.print("address of first  string is 0x");
  Serial.println((unsigned) s1, 16);
  Serial.print("address of second string is 0x");
  Serial.println((unsigned) s2, 16);

  // compare the strings' memory addresses
  if (s1 == s2) {
    Serial.println("string memory addresses equal");
  } else {
    Serial.println("string memory addresses not equal");
  }

  // compare the strings' contents
  if (strcmp(s1, s2) == 0) {
    Serial.println("string contents match");
  } else {
    Serial.println("string contents don't match");
  }

  // add a blank line to visually separate output
  Serial.println();
}

// called once at startup of execution
void
setup()
{
  // initialize serial communications
  Serial.begin(9600);

  // copy the contents of string x into string z
  strcpy(z, x);

  // compare strings x and y: should have the same address and contents
  Serial.println("comparing strings x and y:");
  compare(x, y);

  // compare strings x and z: should have different addresses and same contents
  Serial.println("comparing strings x and z:");
  compare(x, z);
}

// called repeatedly during execution
void
loop()
{
  // nothing more to do
}
